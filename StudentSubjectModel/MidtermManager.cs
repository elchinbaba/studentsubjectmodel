﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSubjectModel
{
    class MidtermManager : Manager
    {
        private int midtermIsHappened;
        private int[] scores;

        public MidtermManager()
        {
            scores = new int[3];
            midtermIsHappened = 0;
        }

        private bool IsValid(MidtermModel midterm)
        {
            return midterm.Score >= 0 && midterm.Score <= 20;
        }

        public void AddMidterm(MidtermModel midterm)
        {
            try
            {
                if (!IsValid(midterm))
                {
                    throw new ArgumentOutOfRangeException("Invalid midterm point");
                }

                scores[midtermIsHappened] = midterm.Score;
                midtermIsHappened++;
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public double GetFinalMidtermScore()
        {
            double sum = 0;
            foreach (int midterm in scores)
            {
                sum += midterm;
            }
            return sum / 3;
        }
    }
}
