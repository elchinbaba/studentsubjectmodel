﻿using System;
using System.Collections.Generic;

namespace StudentSubjectModel
{
    internal class Subject
    {
        public int SubjectId { get; set; }
        public string Name { get; set; }
        public double SubjectHours { get; set; }

        private MidtermManager midterm;
        private LaboratoryManager laboratory;
        private double presentationPoint;
        private double activityPoint;
        private int absentHours;
        private double enterancePoint;

        public Subject(int subjectId, string subjectName, double subjectHours, int labCount)
        {
            SubjectId = subjectId;
            Name = subjectName;
            SubjectHours = subjectHours;

            Midterm = new MidtermManager();
            Laboratory = new LaboratoryManager(labCount);
        }

        public MidtermManager Midterm
        {
            get
            {
                return midterm;
            }
            private set
            {
                midterm = value;
            }
        }

        public LaboratoryManager Laboratory
        {
            get
            {
                return laboratory;
            }
            private set
            {
                laboratory = value;
            }
        }

        public double PresentationPoint
        {
            get
            {
                return presentationPoint;
            }
            set
            {
                try
                {
                    if (value >= 0 && value <= 10)
                    {
                        presentationPoint = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("Invalid presentation point");
                    }

                }
                catch (ArgumentOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public double ActivityPoint
        {
            get
            {
                return activityPoint;
            }
            set
            {
                try
                {
                    if (value >= 0 && value <= 10)
                    {
                        activityPoint = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("Invalid activity point");
                    }

                }
                catch (ArgumentOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        // absent count add
        public int IsAbsent()
        {
            absentHours += 2;

            return absentHours / 2;
        }

        private bool CheckLimit()
        {
            if ((SubjectHours / 2) * 0.25 <= absentHours)
            {
                return true;
            }
            return false;
        }

        public double EnterancePointCalculator()
        {
            try
            {
                enterancePoint = Midterm.GetFinalMidtermScore() + Laboratory.GetFinalLaboratoryScore() + PresentationPoint + ActivityPoint;

                if (CheckLimit())
                {
                    throw new Exception("Subject failed");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("You are on the limit. You cannot enter the exam!");
            }
            return enterancePoint;
        }
    }
}