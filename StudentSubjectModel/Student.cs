﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSubjectModel
{
    internal class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Faculty { get; set; }
        public string Speciality { get; set; }
        private List<Subject> Subjects;

        public Student(int id, string name, string surname, string faculty, string speciality)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Faculty = faculty;
            Speciality = speciality;

            Subjects = new List<Subject>();
        }

        public void AddSubject(Subject newSubject)
        {
            Subjects.Add(newSubject);
        }

        public void RemoveSubject(Subject deleteSubject)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.SubjectId == deleteSubject.SubjectId)
                {
                    Subjects.Remove(subject);
                    break;
                }
            }
        }

        // show subjects method 

        public void AddMidtermToSubject(string subjectName, int score)
        {
            MidtermModel midterm = new MidtermModel();
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    midterm.Score = score;
                    subject.Midterm.AddMidterm(midterm);
                    return;
                }
            }
        }

        public double GetFinalMidtermScoreForSubject(string subjectName)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    return subject.Midterm.GetFinalMidtermScore();
                }
            }
            return 0;
        }

        public void AddLabToSubject(string subjectName, int score)
        {
            LaboratoryModel laboratory = new LaboratoryModel();
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    subject.Laboratory.AddLaboratory(laboratory);
                    return;
                }
            }
        }

        public double GetFinalLabScoreForSubject(string subjectName)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    return subject.Laboratory.GetFinalLaboratoryScore();
                }
            }
            return 0;
        }

        //presentation and activity
        public void AddPresentationPointToSubject(string subjectName, int score)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    subject.PresentationPoint = score;
                    break;
                }
            }
        }

        public double GetPresentationPointForSubject(string subjectName)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    return subject.PresentationPoint;
                }
            }
            return 0;
        }

        public void AddActivityPointToSubject(string subjectName, int score)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    subject.ActivityPoint = score;
                    break;
                }
            }
        }

        public double GetActivityPointForSubject(string subjectName)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    return subject.ActivityPoint;
                }
            }
            return 0;
        }

        public double GetEnterancePointForSubject(string subjectName)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    return subject.EnterancePointCalculator();
                }
            }
            return 0;
        }

        // absent or not 
        public int AddAbsentToSubject(string subjectName)
        {
            foreach (Subject subject in Subjects)
            {
                if (subject.Name == subjectName)
                {
                    return subject.IsAbsent();
                }
            }
            return 0;
        }

    }
}