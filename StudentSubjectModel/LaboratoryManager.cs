﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSubjectModel
{
    class LaboratoryManager
    {
        private int labIsHappened;
        private int[] scores;

        public int LabCount { get; set; }

        public LaboratoryManager(int labCount)
        {
            LabCount = labCount;
            scores = new int[labCount];
            labIsHappened = 0;
        }

        private bool IsValid(LaboratoryModel laboratory)
        {
            return laboratory.Score >= 0 && laboratory.Score <= 10;
        }
        public void AddLaboratory(LaboratoryModel laboratory)
        {
            try
            {
                if (!IsValid(laboratory))
                {
                    throw new ArgumentOutOfRangeException("Invalid laboratory point. should be between 0 and 10");
                }

                scores[labIsHappened] = laboratory.Score;
                labIsHappened++;
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public double GetFinalLaboratoryScore()
        {
            double sum = 0;
            foreach (int labs in scores)
            {
                sum += labs;
            }
            return sum / LabCount;
        }
    }
}
